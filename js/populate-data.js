var dataParentContainer = document.querySelector("#data-container");

function produceDataItem(text, url, target) {
  var item = document.createElement("a");
  item.setAttribute("class", "data-item");
  item.setAttribute("target", target);
  item.href = url;
  item.innerHTML = text;
  return item;
}

// a = JSON.parse(atob(""));
// a[0].url = "";
// btoa(JSON.stringify(a));

JSON.parse(atob(
/*blob*/"WwogIHsKICAgICJ0aXRsZSI6ICJDViIsCiAgICAidXJsIjogInN0YXRpYy9jdi9NYXJ0eW5hcy1NYWNpdWxldmljaXVzXzIwMjVjLnBkZiIsCiAgICAidGFyZ2V0IjogIl9ibGFuayIKICB9LAogIHsKICAgICJ0aXRsZSI6ICJMaW5rZWRJbiIsCiAgICAidXJsIjogImh0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9tYXJ0eW5hcy1tYWNpdWxldmljaXVzLyIsCiAgICAidGFyZ2V0IjogIl9ibGFuayIKICB9LAogIHsKICAgICJ0aXRsZSI6ICJHaXRMYWIiLAogICAgInVybCI6ICJodHRwczovL2dpdGxhYi5jb20vSW52ZXJ0aXNtZW50IiwKICAgICJ0YXJnZXQiOiAiX2JsYW5rIgogIH0sCiAgewogICAgInRpdGxlIjogIkdpdEh1YiIsCiAgICAidXJsIjogImh0dHBzOi8vZ2l0aHViLmNvbS9JbnZlcnRpc21lbnQiLAogICAgInRhcmdldCI6ICJfYmxhbmsiCiAgfSwKICB7CiAgICAidGl0bGUiOiAiTWFpbCIsCiAgICAidXJsIjogIm1haWx0bzptYXJ0eW5hcy5tYWNpdWxldmljaXVzK21tYWNpdWwubHRAcG0ubWUiLAogICAgInRhcmdldCI6ICJfc2VsZiIKICB9Cl0K"/*blob*/
)).forEach(item => {
  dataParentContainer.appendChild(
    produceDataItem(
      item.title,
      item.url,
      item.target));
});

const appsInfo = [
  {
    "title": "Clojure events calendar UI",
    "href": "https://invertisment.gitlab.io/cljcalendar/",
    "body": [
      {
        "text": "Web preview of upcoming Clojure events."
      }
    ],
    "imgSrc": "img/cljcalendar.png",
  },
  {
    "title": "Tetris in browser",
    "href": "https://invertisment.gitlab.io/cljs-tetris/",
    "body": [
      {
        "text": "Simplistic tetris game written in ClojureScript and transpiled to JavaScript. Playable only with a keyboard. "
      }
    ],
    "imgSrc": "img/tetris.png",
  },
  {
    "title": "EOS block producer browser",
    "href": "https://invertisment.gitlab.io/honest-producers/",
    "body": [
      {
        "text": "Explore summarized data of EOS block producers. Vote for your favourite block producers through "
      },
      {
        "href": "https://github.com/CryptoLions/EOS-MainNet/blob/master/cleos.sh",
        "text": "cleos script"
      },
      {
        "text": "."
      }
    ],
    "imgSrc": "img/producers.png",
  },
]

var appsParentContainer = document.querySelector("#apps-container");

function toDescription(body) {
  const a = body.map((bodyItem) => {
    if (bodyItem.href) {
      return `<a href="${bodyItem.href}" target="_blank">${bodyItem.text}</a>`
    }
    return bodyItem.text
  }).reduce((out, item) => out.concat(item), "")
  console.log("data", a)
  return a
}

function produceAppsDataItem(title, href, body, imgSrc) {
  var item = document.createElement("div");
  item.className = "mui-col-md-6"
  item.innerHTML = `<div class="mui-container mui-panel">
    <h2>
      <a href="${href}" target="_blank">
        ${title}
      </a>
    </h2>
    <p>
      ${toDescription(body)}
    </p>
    <a href="${href}" target="_blank">
      <img src="${imgSrc}" class="card-img"></img>
    </a>
  </div>`;
  return item;
}

appsParentContainer.innerHTML = ''
appsInfo.forEach(({ title, href, body, imgSrc }) => {
  appsParentContainer.appendChild(
    produceAppsDataItem(title, href, body, imgSrc));
});
