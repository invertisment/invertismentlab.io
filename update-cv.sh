
JSFILE="js/populate-data.js"
TEMPFILE="temp.js"
CV_DIR="static/cv"
CV_LOC="${CV_DIR}/$(ls ${CV_DIR} | sort | tail -n 1)"

echo $CV_LOC

##### Put JSON blob into JS file #####

NEW_VAL=$(cat $JSFILE | grep "\*blob\*" | sed -e 's/\/\*blob\*\///g' -e 's/[" ]//g' | base64 --decode | jq ".[0].url |= \"${CV_LOC}\"" | base64 -w 0)

cat js/populate-data.js | sed -e "s/blob.*blob/blob*\/\"$NEW_VAL\"\/*blob/g" > $TEMPFILE

mv $TEMPFILE $JSFILE
